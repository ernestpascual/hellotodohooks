import React from 'react';

const todo = (props) => {
    // You can use Hooks here!
    
    return (
    <div>
            {props.items.map( (item)=> (
            <div >
                <h1> { item.title } </h1>
                <div >
                <p> { item.desc } </p>
                <p> { item.completed } </p>
                </div>
                <button onClick={ props.delete } > delete </button>
            </div>
            ))}
    </div>
    ) 
  }

export default todo;