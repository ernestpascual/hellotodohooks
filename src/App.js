import React, {useState} from 'react';

import './App.css';
import Todo from './todo'

function App() {
  
  const[todo, setTodo] = useState([
     {title: "Hello",
     desc: "desc",
    completed: "complete"},

    {title: "Hi",
      desc: "Pota",
       completed: "complete"},

    ]);
  const[value, setValue] = useState("");

  // store value
  const store = event => {
    event.preventDefault();
    setValue(event.target.value)
  }

  // add new task
  const addTask = event => {
    event.preventDefault();
    setTodo([...todo, {todo: value}])
    event.target.value = ""
  }

  // TODO: bug --- delete the latest array
  const deleteTask = (event, index) => {
    event.preventDefault();
    console.log(index)
    const newTasks = [...todo];
    newTasks.splice(index, 1);
    setTodo(newTasks);
  }
 
  return (
    <div>
      <h1> To Do List</h1>
      <input type="text" onChange={store}/>
      <button onClick={addTask}>
        Add
      </button>
      <button onClick={()=> setTodo([])}>
        Reset
      </button>
      <Todo items= {todo} delete={deleteTask} />
    </div>
  );
}

export default App;

// TODO:
// [] add css
// [] fix delete bug
// [] add completed
